const path = require('path');

module.exports = function cookies (_options) {
  const defaultOptions = {
    debounce: 100,
    breakpoints: {
      'mobile': '<=768',
      'tablet': ['<=1024', '>768'],
      'desktop': '>768'
    }
  }

  this.addPlugin({
    src: path.resolve(__dirname, 'plugin.js'),
    fileName: 'breakpoints.js',
    options: Object.assign(defaultOptions, _options)
  })
}

module.exports.meta = require('../package.json')