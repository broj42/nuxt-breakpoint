import Vue from 'vue';
let options = <%= serialize(options) %>

export default (context, inject) =>{
  let breakpoints = {}
  let resizeTimeout = null;

  const setBreakpoints = (debounce=0) =>{
    clearTimeout(resizeTimeout)
    resizeTimeout = setTimeout(() => {
      for(let key in options.breakpoints){
        let isBreakpoint = false;
        if(Array.isArray(options.breakpoints[key])){
          isBreakpoint = options.breakpoints[key].every(item => eval(`${window.innerWidth}${item}`))
        } else isBreakpoint = eval(`${window.innerWidth}${options.breakpoints[key]}`)
        Vue.set(breakpoints, key, isBreakpoint)
      }
      inject('breakpoints', breakpoints)
    }, debounce);
  }

  if(process.browser){
    window.onNuxtReady(() => setBreakpoints())
    window.addEventListener('resize', () => setBreakpoints(options.debounce));
    inject('breakpoints', breakpoints)
  }
}
