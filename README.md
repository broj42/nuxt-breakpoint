# Nuxt Breakpoint
### 🚀 Usage
```bash
npm i nuxt-breakpoint
```
```javascript
//nuxt.config.js
modules: [
  ['nuxt-breakpoint', {
    //options
  }]
]
```
This module is working on **client side** only.
Variable **$breakpoints** is available everywhere, to make it reactive, you need to add it to the page/component data:
```javascript
data() {
  return {
    breakpoints: this.$breakpoints
  }
}
```


### 🔧 Options
- breakpoints
- debounce

#### Breakpoints
You can add any amount of breakpoints you want:
```javascript
modules: [
  ['nuxt-breakpoint', {
    breakpoints: {
      // Default options
      'mobile': '<=768',
      'tablet': ['<=1024', '>768'],
      'desktop': '>1024'
    }
  }]
]
```

#### Debounce
Number of miliseconds to debounce window '**resize**' event (default: 100)

